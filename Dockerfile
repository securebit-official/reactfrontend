#Build code and produce /app/build
FROM tiangolo/node-frontend:10 as build-stage
WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY ./ /app/
RUN npm run build

#Build docker Image with nginx and build directory
FROM nginx:1.15
COPY --from=build-stage /app/build/ /usr/share/nginx/html
# Copy the default nginx.conf provided by tiangolo/node-frontend

#TO DO - fornire un file di nginx conf da per la configurazione fisica del http server
#COPY --from=build-stage /nginx.conf /etc/nginx/conf.d/default.conf

#commento